# Develop online

Start hacking in no time, just click here:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/moduon/odoo-module-template)

# Develop locally

We use devenv, so [follow upstream setup guide](https://devenv.sh/getting-started/).

Once done, integrate it better
[installing direnv](https://direnv.net/docs/installation.html) and
[configuring it for your shell](https://direnv.net/docs/hook.html). Don't forget to run
`direnv allow` once all that is done.

Finally, get even better integration by using VSCode/VSCodium and installing the
recommended extensions.
