# Get precommix from separate location, synced with last template update
import (builtins.fetchGit {
  url = "https://gitlab.com/moduon/precommix.git";
  ref = "main";
  rev = "98014687649e8f7272fc2db97fb93a4b1c47ce8a";
})
