#!/usr/bin/python
"""Print a YAML list of all Odoo categories.

This file must be executed from the root folder of a checkout of Odoo source code.
"""

import ast
from contextlib import suppress
from pathlib import Path

categories = {"Uncategorized"}

for manifest_path in Path().glob("addons/*/__manifest__.py"):
    manifest_text = manifest_path.read_text()
    parsed = ast.literal_eval(manifest_text)
    with suppress(KeyError):
        categories.add(parsed["category"])

for category in sorted(categories, key=lambda c: c.lower()):
    print(f"- {category}")
