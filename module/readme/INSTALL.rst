..  This file must only be present if there are very specific
    installation instructions, such as installing non-python
    dependencies. The audience is systems administrators.

    You don't need to specify python or odoo dependencies. Odoo will raise an
    error automatically with a specific message if one of these are missing.

To install this module, you need to:

#. Do this ...
