..  This file is optional. It should explain how to configure
    the module before using it. It is aimed at advanced users.

To configure this module, you need to:

#. Go to ...

.. figure:: ../static/description/image.png
   :alt: alternative description
   :width: 600 px
